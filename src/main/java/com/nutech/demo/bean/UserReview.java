package com.nutech.demo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Hetesh Mohan
 * @desc  user Review bean
 *
 */
@Entity
@Table(name="user_review")
public class UserReview {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int reviewId;

	@Column(name="user_id")
	private int userId;
	
	@Column(name="order_id")
	private int orderId;
	
	@Column(name="rating")
	private int rating;
	
	@Column(name="comments")
	private String comments;

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	

}
