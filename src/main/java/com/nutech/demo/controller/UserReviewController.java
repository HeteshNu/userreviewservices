package com.nutech.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nutech.demo.bean.UserReview;
import com.nutech.demo.service.ReviewService;


/**
 * @author Hetesh Mohan
 *
 */
@RestController
@RequestMapping(value={"/review"})
public class UserReviewController {
	
	public static final String CREATED = "created";
	public static final String  CONFLICT = "Unable to Create";
	@Autowired
	ReviewService reviewService;

	
	/**
	 * @detail submit user view and its details
	 * @param review
	 * @return
	 */
	@PostMapping(value="/create",headers="Accept=application/json")
	public ResponseEntity<String> SubmitUserReview(@RequestBody UserReview review)
	{
		  ResponseEntity<String> response = null;
		  boolean status = reviewService.createReview(review);
		  System.out.println(status);
		  if(status==true)
		  {
		  response = new ResponseEntity<String>(CREATED,HttpStatus.CREATED);
		  }else
		  {
			  response = new ResponseEntity<String>(CONFLICT,HttpStatus.OK);
		  }
		  return response;
		  
	}
}

